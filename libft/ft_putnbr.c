/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mnosko <mnosko@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/27 16:51:20 by mnosko            #+#    #+#             */
/*   Updated: 2017/11/07 11:41:54 by mnosko           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_putnbr(int n)
{
	int div;
	int negative;

	div = 1;
	negative = 1;
	if (n < 0)
	{
		div = -1;
		negative = -1;
		ft_putchar('-');
	}
	while ((negative == -1 && div * 10 > n && div * 10 >= -1000000000)
			|| (negative == 1 && div * 10 < n && div < 1000000000))
	{
		div *= 10;
	}
	while ((negative == 1 && div >= 10) || (negative == -1 && div <= -10))
	{
		ft_putchar((n / div % 10) + '0');
		div /= 10;
	}
	ft_putchar((n % 10) * negative + '0');
}
