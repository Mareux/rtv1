/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strequ.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mnosko <mnosko@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/26 17:59:38 by mnosko            #+#    #+#             */
/*   Updated: 2017/11/07 14:34:31 by mnosko           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	ft_strequ(char const *s1, char const *s2)
{
	int i;

	i = 0;
	if (s1 == NULL)
		return (0);
	else if (s2 == NULL)
		return (0);
	while (s1[i])
	{
		if (s1[i] != s2[i])
			return (0);
		if (s2[i] == '\0')
			break ;
		i++;
	}
	if (s1[i] != s2[i])
		return (0);
	return (1);
}
