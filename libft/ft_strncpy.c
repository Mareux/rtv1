/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mnosko <mnosko@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/25 16:46:20 by mnosko            #+#    #+#             */
/*   Updated: 2017/11/06 11:06:13 by mnosko           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strncpy(char *dst, const char *src, size_t len)
{
	size_t	lens;
	int		i;
	int		k;

	lens = ft_strlen(src);
	i = -1;
	k = -1;
	while (++i < (int)len)
	{
		if ((++k < (int)lens) && src[k])
			dst[i] = src[k];
		else
			dst[i] = '\0';
	}
	return (dst);
}
