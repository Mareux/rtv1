/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mnosko <mnosko@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/25 14:06:40 by mnosko            #+#    #+#             */
/*   Updated: 2017/11/02 16:27:01 by mnosko           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memchr(const void *s, int c, size_t n)
{
	const unsigned char		*str = s;
	unsigned char			cc;
	int						i;

	i = -1;
	cc = c;
	while (++i < (int)n)
		if (str[i] == cc)
		{
			return ((void*)&s[i]);
		}
	return (0);
}
