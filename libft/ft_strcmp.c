/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mnosko <mnosko@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/26 11:32:39 by mnosko            #+#    #+#             */
/*   Updated: 2017/11/07 14:34:08 by mnosko           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_strcmp(const char *s1, const char *s2)
{
	int						i;
	const unsigned char		*str1;
	const unsigned char		*str2;

	i = 0;
	str1 = (unsigned char *)s1;
	str2 = (unsigned char *)s2;
	while (str1[i] || str2[i])
	{
		if (str1[i] != str2[i])
			return ((unsigned)str1[i] - (unsigned)str2[i]);
		i++;
	}
	if (str1[i] != '\0')
		return ((unsigned)str1);
	if (str2[i] != '\0')
		return (-(unsigned)str2);
	return (0);
}
