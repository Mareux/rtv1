/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncmp.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mnosko <mnosko@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/26 12:32:57 by mnosko            #+#    #+#             */
/*   Updated: 2017/11/07 17:01:19 by mnosko           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_strncmp(const char *s1, const char *s2, size_t n)
{
	int						i;
	const unsigned char		*str1;
	const unsigned char		*str2;

	i = 0;
	str1 = (unsigned char *)s1;
	str2 = (unsigned char *)s2;
	if (n == 0)
		return (0);
	while (str1[i] != '\0' && str2[i] != '\0')
	{
		if (str1[i] != str2[i])
			return ((unsigned)str1[i] - str2[i]);
		if (i == (int)n - 1)
			return (0);
		i++;
	}
	if (str1[i] != '\0')
		return ((unsigned)str1[i]);
	if (str2[i] != '\0')
		return (-(unsigned)str2[i]);
	return (0);
}
