/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mnosko <mnosko@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/24 18:44:18 by mnosko            #+#    #+#             */
/*   Updated: 2017/11/06 11:43:30 by mnosko           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memcpy(void *dst, const void *src, size_t n)
{
	const unsigned char		*str = src;
	size_t					i;
	unsigned char			*destination;

	i = -1;
	destination = (unsigned char *)dst;
	while (++i < n)
	{
		destination[i] = str[i];
	}
	dst = destination;
	return (dst);
}
