/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstdel.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mnosko <mnosko@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/06 10:57:03 by mnosko            #+#    #+#             */
/*   Updated: 2017/11/07 13:57:46 by mnosko           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstdel(t_list **alst, void (*del)(void *, size_t))
{
	t_list *curr;
	t_list *next;

	curr = *alst;
	while (curr)
	{
		next = curr->next;
		(*del)(curr->content, curr->content_size);
		free(next);
		curr = next;
	}
	free(*alst);
	*alst = NULL;
}
