/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mnosko <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/27 11:55:33 by mnosko            #+#    #+#             */
/*   Updated: 2018/04/23 11:02:46 by mnosko           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int			ft_has_ch(char const *s, char c)
{
	unsigned int i;

	if (!s || !c)
		return (0);
	i = 0;
	while (s[i])
	{
		if (s[i] != c && s[i] != 0)
			return (1);
		++i;
	}
	return (0);
}

static size_t		ft_words(char const *s, char c)
{
	size_t w_count;
	size_t is_not_c;
	size_t i;

	i = 0;
	is_not_c = 0;
	w_count = 0;
	while (i < ft_strlen(s))
	{
		while (s[i] != c && s[i] != '\0')
			++i && ++is_not_c;
		if (is_not_c)
			++w_count;
		++i;
		is_not_c = 0;
	}
	if (w_count == 0 && ft_has_ch(s, c))
		return (1);
	return (w_count);
}

char				**ft_strsplit(char const *s, char c)
{
	char	**mem;
	size_t	start;
	size_t	i;
	size_t	mem_i;

	mem_i = 0;
	i = 0;
	start = 0;
	if (!s || !c)
		return (NULL);
	if ((mem = (char **)ft_memalloc((ft_words(s, c) + 1)
									* sizeof(char*))) == NULL)
		return (NULL);
	while (mem_i < ft_words(s, c))
	{
		while (s[i] == c)
			++i;
		start = i;
		while (s[i] != c && s[i] != '\0')
			i++;
		if ((mem[mem_i++] = ft_strsub(s, start, i - start)) == NULL)
			return (ft_arrdel(mem, ft_words(s, c)) ? (NULL) : (NULL));
		i++;
	}
	return (mem);
}
