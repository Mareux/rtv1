/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mnosko <mnosko@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/27 13:40:23 by mnosko            #+#    #+#             */
/*   Updated: 2017/11/02 16:26:48 by mnosko           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int		count_everyting(int n, unsigned int *del, int *i, int *minus)
{
	int j;

	j = 1;
	if (n < 0)
	{
		*minus = 1;
		*i = *i + 1;
		n *= -1;
	}
	else
		*minus = 0;
	j = n / *del;
	while (j != 0)
	{
		*del = *del * 10;
		j = n / *del;
		*i = *i + 1;
		if (*del > 1000000000)
		{
			*del = 1000000000;
			return (n);
		}
	}
	*del = *del / 10;
	return (n);
}

static char		*returrn_null(void)
{
	char *arr;

	arr = (char*)malloc(sizeof(char) * 2);
	arr[0] = '0';
	arr[1] = '\0';
	return (arr);
}

char			*ft_itoa(int n)
{
	unsigned int	del;
	int				minus;
	int				i;
	char			*number;
	int				j;

	i = 0;
	del = 1;
	minus = 0;
	n = count_everyting(n, &del, &i, &minus);
	if (n == 0)
		return (returrn_null());
	if ((number = (char*)malloc(sizeof(char) * (i + 1))) == NULL)
		return (NULL);
	j = -1;
	if (minus == 1)
		number[++j] = '-';
	while (++j < i)
	{
		number[j] = ((n / del + 48));
		n %= del;
		del /= 10;
	}
	number[j] = '\0';
	return (number);
}
