/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mnosko <mnosko@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/26 19:18:32 by mnosko            #+#    #+#             */
/*   Updated: 2017/11/02 16:29:57 by mnosko           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strtrim(char const *s)
{
	int last;
	int first;

	if (s == NULL)
		return (NULL);
	last = ft_strlen(s) - 1;
	first = 0;
	while (s[first] == ' ' || s[first] == '\t' || s[first] == '\n')
		first++;
	if (first - 1 == last)
		return (ft_strsub(s, (unsigned int)first, (size_t)last - first + 1));
	while (s[last] == ' ' || s[last] == '\t' || s[last] == '\n')
		last--;
	return (ft_strsub(s, (unsigned int)first, (size_t)last - first + 1));
}
