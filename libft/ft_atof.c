/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atof.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mnosko <mnosko@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/26 12:41:11 by mnosko            #+#    #+#             */
/*   Updated: 2017/11/07 17:37:54 by mnosko           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void		help_atof(const char **p, 	double *sign)
{
	while (white_space(**p))
		*p += 1;
	*sign = 1.0;
	if (**p == '-')
	{
		*sign = -1.0;
		*p += 1;
	}
	else if (**p == '+')
		*p += 1;
}

double		ft_atof (const char *p)
{
	double	sign;
	double	value;
	double	pow10;

	help_atof(&p, &sign);
	value = 0.0;
	while (valid_digit(*p))
	{
		value = value * 10.0 + (*p - '0');
		p += 1;
	}
	if (*p == '.')
	{
		pow10 = 10.0;
		p += 1;
		while (valid_digit(*p))
		{
			value += (*p - '0') / pow10;
			pow10 *= 10.0;
			p += 1;
		}
	}
	return sign * value;
}

