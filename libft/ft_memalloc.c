/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memalloc.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mnosko <mnosko@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/26 14:10:22 by mnosko            #+#    #+#             */
/*   Updated: 2017/11/07 13:51:54 by mnosko           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memalloc(size_t size)
{
	unsigned char	*memory;
	int				i;

	i = -1;
	memory = (unsigned char*)malloc(sizeof(char) * size);
	if (memory == NULL)
		return (NULL);
	while (++i < (int)size)
		memory[i] = '\0';
	return ((void*)memory);
}
