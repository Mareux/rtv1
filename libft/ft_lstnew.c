/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mnosko <mnosko@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/06 10:59:32 by mnosko            #+#    #+#             */
/*   Updated: 2017/11/07 14:29:25 by mnosko           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list	*ft_lstnew(void const *content, size_t content_size)
{
	t_list	*new;
	void	*tmp;

	if ((new = (t_list*)malloc(sizeof(t_list))) == NULL)
		return (NULL);
	if ((new->content = malloc(content_size)) == NULL)
		return (NULL);
	if (content == NULL)
	{
		new->content = NULL;
		new->content_size = 0;
		new->next = 0;
		return (new);
	}
	new->content_size = content_size;
	tmp = new->content;
	while (content_size--)
	{
		*((unsigned char*)new->content++) = *((unsigned char*)content++);
	}
	new->content -= new->content - tmp;
	new->next = 0;
	return (new);
}
