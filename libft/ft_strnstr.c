/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mnosko <mnosko@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/26 12:02:00 by mnosko            #+#    #+#             */
/*   Updated: 2017/11/07 17:55:30 by mnosko           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strnstr(const char *big, const char *little, size_t len)
{
	size_t i;
	size_t k;
	size_t lenn;

	i = 0;
	k = 0;
	lenn = ft_strlen(little);
	if (lenn == 0)
		return ((char*)big);
	while (i < len && big[i])
	{
		if (big[i] == little[k])
		{
			k++;
			if (k == lenn)
				return ((char*)&big[i - k + 1]);
		}
		else if (k > 0)
		{
			i = i - k;
			k = 0;
		}
		i++;
	}
	return (0);
}
