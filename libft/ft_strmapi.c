/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmapi.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mnosko <mnosko@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/26 17:51:32 by mnosko            #+#    #+#             */
/*   Updated: 2017/11/07 14:34:56 by mnosko           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strmapi(char const *s, char (*f)(unsigned int, char))
{
	unsigned int		len;
	int					i;
	char				*str;

	if (s == NULL)
		return (NULL);
	len = ft_strlen(s);
	i = -1;
	if ((str = (char*)malloc(sizeof(char) * len + 1)) == NULL)
		return (NULL);
	while (s[++i])
		str[i] = (*f)((unsigned int)i, s[i]);
	str[i] = '\0';
	return (str);
}
