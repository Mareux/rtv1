/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mnosko <mnosko@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/25 18:36:24 by mnosko            #+#    #+#             */
/*   Updated: 2017/11/06 11:02:47 by mnosko           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strchr(const char *s, int c)
{
	unsigned char	cc;
	int				i;

	i = -1;
	cc = c;
	while (s[++i])
		if (s[i] == cc)
		{
			return ((char*)&s[i]);
		}
	if (s[i] == cc)
		return ((char*)&s[i]);
	return (0);
}
