/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mnosko <mnosko@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/24 20:24:26 by mnosko            #+#    #+#             */
/*   Updated: 2017/11/07 14:32:10 by mnosko           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memmove(void *dst, const void *src, size_t len)
{
	const unsigned char	*source = src;
	unsigned char		*destination;
	int					lenght;

	destination = dst;
	lenght = len;
	if (source >= destination)
		return (ft_memcpy(dst, src, len));
	while (len)
	{
		source++;
		destination++;
		len--;
	}
	destination--;
	source--;
	while (lenght > 0)
	{
		*destination = *source;
		destination--;
		source--;
		lenght--;
	}
	return (dst);
}
