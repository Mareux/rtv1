/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr_fd.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mnosko <mnosko@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/02 13:02:58 by mnosko            #+#    #+#             */
/*   Updated: 2017/11/07 11:46:03 by mnosko           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_putnbr_fd(int n, int fd)
{
	int div;
	int negative;

	div = 1;
	negative = 1;
	if (n < 0)
	{
		div = -1;
		negative = -1;
		ft_putchar_fd('-', fd);
	}
	while ((negative == -1 && div * 10 > n && div * 10 >= -1000000000)
			|| (negative == 1 && div * 10 < n && div < 1000000000))
	{
		div *= 10;
	}
	while ((negative == 1 && div >= 10) || (negative == -1 && div <= -10))
	{
		ft_putchar_fd(((n / div % 10) + '0'), fd);
		div /= 10;
	}
	ft_putchar_fd(((n % 10) * negative + '0'), fd);
}
