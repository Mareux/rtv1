/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mnosko <mnosko@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/24 20:05:12 by mnosko            #+#    #+#             */
/*   Updated: 2017/11/07 17:49:27 by mnosko           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memccpy(void *dst, const void *src, int c, size_t n)
{
	unsigned char		*str;
	unsigned char		*destination;
	size_t				i;

	i = 0;
	destination = (unsigned char*)dst;
	str = (unsigned char*)src;
	while (i < n)
	{
		destination[i] = str[i];
		if (str[i] == (unsigned char)c)
		{
			return (unsigned char*)&destination[i + 1];
		}
		i++;
	}
	return (NULL);
}
