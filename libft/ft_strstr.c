/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mnosko <mnosko@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/25 18:49:55 by mnosko            #+#    #+#             */
/*   Updated: 2017/11/06 14:46:19 by mnosko           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strstr(const char *big, const char *little)
{
	int i;
	int k;
	int len;

	i = 0;
	k = 0;
	len = ft_strlen(little);
	if (len == 0)
		return ((char*)big);
	while (big[i])
	{
		if (big[i] == little[k])
		{
			k++;
			if (k == len)
				return ((char*)&big[i - k + 1]);
		}
		else if (k > 0)
		{
			i = i - k;
			k = 0;
		}
		i++;
	}
	return (0);
}
