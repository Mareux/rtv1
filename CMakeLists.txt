cmake_minimum_required(VERSION 3.12)
project(rtv1)

set(CMAKE_C_STANDARD 11)

set(SDL2_INCLUDE_DIR ~/.brew/include/SDL2/)
set(SDL2_LIBRARY ~/.brew/lib/)
include_directories(${SDL2_INCLUDE_DIR})
link_directories(${SDL2_LIBRARY})

file(GLOB rtv1_lib
        "libft/*.c")

file(GLOB src
        "srcs/*.c")

add_executable(rtv1
        ${rtv1_lib}
        ${src}
        get_next_line.c)
target_link_libraries(rtv1 -lSDL2 -lm)
