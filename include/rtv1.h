/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rtv1.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mnosko <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/24 19:22:27 by mnosko            #+#    #+#             */
/*   Updated: 2018/10/24 19:22:42 by mnosko           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef RTV1_RTV1_H
# define RTV1_RTV1_H
# define WINDOW_W 800
# define WINDOW_H 800
# define TRUE 1
# define FALSE 0
# define PI 3.14
# define T_MIN 1
# define T_MAX 2147483647

# include <math.h>
# include "SDL.h"
# include "../libft/libft.h"

enum					e_figure_type
{
	CONE, CYLINDER, SPHERE, PLANE
};

enum					e_light_type
{
	AMBIENT, POINT, NONE
};

typedef struct			s_vec3
{
	double				x;
	double				y;
	double				z;
}						t_vec3;

typedef struct			s_light_variables
{
	t_vec3				normal;
	t_vec3				direction;
}						t_light_variables;

typedef struct			s_ray
{
	t_vec3				camera;
	t_vec3				direction;
}						t_ray;

typedef struct			s_color
{
	double				r;
	double				g;
	double				b;
}						t_color;

typedef struct			s_figure
{
	enum e_figure_type	type;
	t_vec3				center;
	double				radius;
	double				degree;
	double 				radians;
	t_vec3 				rotation;
	t_vec3				direction;
	t_color				color;
	double				specular;
	struct s_figure		*next;
}						t_figure;

typedef struct			s_light
{
	enum e_light_type	type;
	double				intensity;
	t_vec3				position;
	struct s_light		*next;
}						t_light;

typedef struct			s_t
{
	double				t1;
	double				t2;
}						t_t;

typedef struct			s_rtv1
{
	int					view[2];
	int					d;
	t_vec3				camera_pos;
	t_vec3				camera_rot;
	t_figure			*figure;
	t_light				*light;
}						t_rtv1;

void					init(t_rtv1 **rtv1);
void					pop_light(t_light **begin);
void					parser(int fd, t_rtv1 *rtv1);
void					pop_figure(t_figure **begin, enum e_figure_type type);
int						equals(char *str1, char *str2);
double					dot_product(t_vec3 a, t_vec3 b);
t_vec3					canvas_to_viewport(int x, int y, t_rtv1 *rtv1);
void					ray_intersect_sphere(t_figure *figure, t_t *t,
						t_vec3 camera, t_vec3 direction);
void					ray_intersect_cylinder(t_figure *figure, t_t *t,
						t_vec3 camera, t_vec3 direction);
void					ray_intersect_cone(t_figure *figure, t_t *t,
						t_vec3 camera, t_vec3 direction);
int						add_figure(int fd, t_figure **figure,
						enum e_figure_type type);
int						add_light(int fd, t_light **light);
void					get_light_type(t_light *light, char *value);
t_vec3					minus(t_vec3 a, t_vec3 b);
void					put_pixel(SDL_Surface *surface, int x, int y,
						Uint32 pixel);
void					draw(t_rtv1 *rtv1, SDL_Surface *surface);
unsigned int			create_rgb(t_color color, double i);
t_color					create_color(char *str);
double					calculate_light(t_rtv1 *rtv1, t_light_variables var,
						t_figure *clst_fgr, t_vec3 v);
t_vec3					create_vector_from_str(char *str);
double					vector_length(t_vec3 a);
t_vec3					multiply(t_vec3 vector, double multiplier);
t_vec3					divide(t_vec3 a, double divider);
t_vec3					plus(t_vec3 a, t_vec3 b);
t_figure				*closest_intersection(t_vec3 camera, t_vec3 direction,
						double *closest_t, t_figure *figure);
void					ray_intersect_plane(t_figure *figure, t_t *t,
						t_vec3 camera, t_vec3 direction);
void					rotation_x(t_vec3 *vector, double degree,
						int inversion);
void					rotation_y(t_vec3 *vector, double degree,
						int inversion);
void					rotation_z(t_vec3 *vector, double degree,
						int inversion);
t_vec3					rotate(t_vec3 vector, t_vec3 degree, int inversion);
void					key_hooks(t_rtv1 *rtv1, SDL_Surface *surface,
						SDL_Window *window);
void					ft_delete(char **ar);
int						shadows(t_ray ray, t_figure *figures,
						double t_min, int t_max);
int						if_split_null(char **split, char **line);
void					delete_before_leaving(char **line, char **split);
t_vec3					get_normal(t_vec3 p, t_figure *closest_figure, double closest_t, t_ray ray);
unsigned int			trace_ray(t_vec3 camera, t_vec3 direction,
						t_rtv1 *rtv1);
t_t						current_figure_intersection(t_figure *figure,
						t_vec3 camera, t_vec3 direction);
double					intensity_without_shine(t_vec3 normal,
						t_vec3 light_direction, double i, t_light *light);
double					shn(t_light_variables variables,
						t_figure *closest_figure, t_vec3 v, t_light *light);
t_vec3					get_light_direction(t_light *light,
						int *t_max, t_vec3 intersection);
char					**get_parser_info(char **line, int *gnl, int fd);
int						parser_for_objects(char **split, int fd, t_rtv1 *rtv1);
void					put_error(char *str);
t_vec3					get_sphere_normal(t_vec3 p, t_figure *closest_figure);
t_vec3					get_cylinder_normal(t_vec3 p, t_figure *closest_figure, double closest_t, t_ray ray);
t_vec3					get_cone_normal(t_vec3 p, t_figure *closest_figure, double closest_t, t_ray ray);
void					change_figure_direction(t_figure *figures);
t_vec3					get_plane_normal(t_vec3 direction, t_figure *closest_figure);
int						if_symbol(char **split, char **line, char *str);

#endif
