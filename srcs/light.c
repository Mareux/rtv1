/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   light.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mnosko <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/24 19:18:37 by mnosko            #+#    #+#             */
/*   Updated: 2018/10/24 19:18:40 by mnosko           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/rtv1.h"

double		calculate_light(t_rtv1 *rtv1, t_light_variables var,
							t_figure *clst_fgr, t_vec3 v)
{
	double		i;
	t_light		*light;
	t_vec3		l;
	int			t_max;

	i = 0.0;
	light = rtv1->light;
	while (light)
	{
		if (light->type == AMBIENT)
			i += light->intensity > 0 ? light->intensity : 0.0;
		else
		{
			l = get_light_direction(light, &t_max, var.direction);

			//вот тут бы правильно проверять угол между нормалью и лучем света

			double alpha = dot_product(l, var.normal) / (vector_length(l) * vector_length(var.normal));
			if ((acos(alpha) * (180 / PI)) < 90 && clst_fgr->type == PLANE) {
				light = light->next;
				continue ;
			}

			//конец проверки на угол между нормалью и лучем света

			if (!shadows((t_ray){var.direction, l}, rtv1->figure, 0.001, t_max))
			{
				light = light->next;
				continue;
			}
			i = intensity_without_shine(var.normal, l, i, light);
			i += shn((t_light_variables) {var.normal, l}, clst_fgr, v, light);
		}
		light = light->next;
	}
	return (i);
}

t_vec3		get_light_direction(t_light *light, int *t_max, t_vec3 intersection)
{
	t_vec3	l;

	l = minus(light->position, intersection);
	*t_max = 1;
//	l = divide(l, vector_length(l));
	return (l);
}

double		intensity_without_shine(t_vec3 normal,
			t_vec3 light_direction, double i, t_light *light)
{
	double dot;
//
//	if (vector_length(light_direction) <= figure->radius) {
//		*t = (t_t) {T_MAX, T_MAX};
//		return;
//	}
	if (light->intensity < 0)
		return (0.0);
//	light_direction = divide(light_direction, vector_length(light_direction));
	dot = dot_product(normal, light_direction);
	if (dot > 0)
		i += (light->intensity * dot) / (vector_length(normal)
				* vector_length(light_direction));
	return (i);
}

double		shn(t_light_variables variables,
			t_figure *closest_figure, t_vec3 v, t_light *light)
{
	double	r_dot_v;
	double	i;
	t_vec3	r;

	i = 0.0;
	if (light->intensity < 0)
		return (0.0);
	if (closest_figure->specular == 0)
		closest_figure->specular = 1;
	if (closest_figure->specular > -1)
	{
//		variables.direction = divide(variables.direction, vector_length(variables.direction));
		r = minus(multiply(variables.normal,
				dot_product(variables.normal, variables.direction) * 2),
						variables.direction);
//		v = divide(v, vector_length(v));
		r_dot_v = dot_product(r, v);
		if (r_dot_v > 0)
			i += light->intensity *
					pow(r_dot_v / (vector_length(r) * vector_length(v)),
					closest_figure->specular);
	}
	return (i);
}
