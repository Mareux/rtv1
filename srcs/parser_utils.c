/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser_utils.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mnosko <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/24 19:19:47 by mnosko            #+#    #+#             */
/*   Updated: 2018/10/24 19:19:48 by mnosko           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/rtv1.h"

t_vec3		create_vector_from_str(char *str)
{
	t_vec3	vec3;
	char	**split;
	char	*trim;
	int		i;

	trim = ft_strtrim(str);
	split = ft_strsplit(++trim, ',');
	free(--trim);
	i = 0;
	while (i < 3)
	{
		if (split[i] == NULL)
		{
			ft_delete(split);
			return (create_vector_from_str("[0, 0, 0]"));
		}
		i++;
	}
	vec3.x = ft_atoi(split[0]);
	vec3.y = ft_atoi(split[1]);
	vec3.z = ft_atoi(split[2]);
	ft_delete(split);
	return (vec3);
}

void		pop_light(t_light **begin)
{
	t_light *new;

	new = malloc(sizeof(t_light));
	new->type = 0;
	new->intensity = 0;
	new->position = create_vector_from_str("[0, 0, 0]");
	new->next = *begin;
	*begin = new;
}

void		pop_figure(t_figure **begin, enum e_figure_type type)
{
	t_figure *new;

	new = malloc(sizeof(t_figure));
	new->type = type;
	new->center = create_vector_from_str("[0, 0, 0]");
	new->color = create_color("[0, 0, 0]");
	new->direction = (t_vec3){0, 1, 0};
	new->rotation = (t_vec3){0, 0, 0};
	new->degree = 20;
	new->radians = 0;
	new->radius = 1;
	new->specular = -1;
	new->next = *begin;
	*begin = new;
}

int			equals(char *str1, char *str2)
{
	char *trim;

	trim = ft_strtrim(str1);
	if (ft_strcmp(trim, str2))
	{
		free(trim);
		return (FALSE);
	}
	free(trim);
	return (TRUE);
}
