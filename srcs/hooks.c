/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hooks.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mnosko <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/22 12:05:38 by mnosko            #+#    #+#             */
/*   Updated: 2018/10/24 19:17:42 by mnosko           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/rtv1.h"

void	key_hooks_moves(t_rtv1 *rtv1, SDL_Surface *surface, SDL_Event event)
{
	if (event.key.keysym.sym == SDLK_LEFT)
		rtv1->camera_pos.x -= 1;
	if (event.key.keysym.sym == SDLK_RIGHT)
		rtv1->camera_pos.x += 1;
	if (event.key.keysym.sym == SDLK_UP)
		rtv1->camera_pos.y += 1;
	if (event.key.keysym.sym == SDLK_KP_PLUS)
		rtv1->camera_pos.z += 1;
	if (event.key.keysym.sym == SDLK_KP_MINUS)
		rtv1->camera_pos.z -= 1;
	if (event.key.keysym.sym == SDLK_DOWN)
		rtv1->camera_pos.y -= 1;
	if (event.key.keysym.sym == SDLK_KP_1)
		rtv1->camera_rot.x += 5;
	if (event.key.keysym.sym == SDLK_KP_2)
		rtv1->camera_rot.x -= 5;
	if (event.key.keysym.sym == SDLK_KP_4)
		rtv1->camera_rot.y += 5;
	if (event.key.keysym.sym == SDLK_KP_5)
		rtv1->camera_rot.y -= 5;
	if (event.key.keysym.sym == SDLK_KP_7)
		rtv1->camera_rot.z += 5;
	if (event.key.keysym.sym == SDLK_KP_8)
		rtv1->camera_rot.z -= 5;
	draw(rtv1, surface);
}

void	key_hooks(t_rtv1 *rtv1, SDL_Surface *surface, SDL_Window *window)
{
	SDL_Event event;

	while (1)
	{
		if (SDL_PollEvent(&event) && event.type == SDL_QUIT)
			break ;
		if (event.type == SDL_KEYDOWN)
		{
			if (event.key.keysym.sym == SDLK_ESCAPE)
			{
				break ;
			}
			key_hooks_moves(rtv1, surface, event);
			SDL_UpdateWindowSurface(window);
		}
	}
}
