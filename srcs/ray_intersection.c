/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ray_intersection.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mnosko <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/24 19:20:32 by mnosko            #+#    #+#             */
/*   Updated: 2018/10/24 19:20:33 by mnosko           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/rtv1.h"

void		ray_intersect_cone(t_figure *figure, t_t *t,
			t_vec3 camera, t_vec3 direction)
{
//	t_vec3	vector;
	double	a;
	double	b;
	double	c;
	double	discr;

//	vector = minus(camera, figure->center);
	if (((figure->degree) > 140.0) || figure->degree < -140)
	{
		*t = (t_t) {T_MAX, T_MAX};
		return;
	}
	figure->radians = (tan((figure->degree / 2) * (PI/180)));
	a = dot_product(direction, direction) -
			(1 + figure->radians * figure->radians)
			* pow(dot_product(direction, figure->direction), 2);
	b = 2 * (dot_product(direction, camera) - (1 + figure->radians
	* figure->radians) * dot_product(direction, figure->direction)
			* dot_product(camera, figure->direction));
	c = dot_product(camera, camera) - ((1 + figure->radians
			* figure->radians) * pow(dot_product(camera, figure->direction), 2));
	discr = b * b - 4 * a * c;
	if (discr < 0)
	{
		t->t1 = T_MAX;
		t->t2 = T_MAX;
		return ;
	}
	t->t1 = (-b + sqrt(discr)) / (2 * a);
	t->t2 = (-b - sqrt(discr)) / (2 * a);
}

void		ray_intersect_cylinder(t_figure *figure, t_t *t,
			t_vec3 camera, t_vec3 direction)
{
//	t_vec3	vector;
	double	a;
	double	b;
	double	c;
	double	discr;

//	vector = minus(camera, figure->center);
	if (vector_length(camera) <= figure->radius)
	{
		*t = (t_t) {T_MAX, T_MAX};
		return;
	}
	a = dot_product(direction, direction)
			- pow(dot_product(direction, figure->direction), 2);
	b = 2 * (dot_product(direction, camera) - dot_product(direction,
			figure->direction) * dot_product(camera, figure->direction));
	c = dot_product(camera, camera) - pow(dot_product(camera,
			figure->direction), 2) - figure->radius * figure->radius;
	discr = b * b - 4 * a * c;
	if (discr < 0)
	{
		*t = (t_t) {T_MAX, T_MAX};
		return ;
	}
	*t = (t_t) {(-b + sqrt(discr)) / (2 * a), (-b - sqrt(discr)) / (2 * a)};
}

void		ray_intersect_sphere(t_figure *figure, t_t *t,
			t_vec3 camera, t_vec3 direction)
{
//	t_vec3	vector;
	double	k1;
	double	k2;
	double	k3;
	double	disctiminant;

//	vector = minus(camera, figure->center);
	if (vector_length(camera) <= figure->radius) {
		*t = (t_t) {T_MAX, T_MAX};
		return;
	}
	k1 = dot_product(direction, direction);
	k2 = 2 * dot_product(camera, direction);
	k3 = dot_product(camera, camera) - figure->radius
			* figure->radius;
	disctiminant = k2 * k2 - 4 * k1 * k3;
	if (disctiminant < 0)
	{
		t->t1 = T_MAX;
		t->t2 = T_MAX;
		return ;
	}
	t->t1 = (-k2 + sqrt(disctiminant)) / (2 * k1);
	t->t2 = (-k2 - sqrt(disctiminant)) / (2 * k1);
}

void		ray_intersect_plane(t_figure *figure, t_t *t,
			t_vec3 camera, t_vec3 direction)
{
//	t_vec3 vector;
	double d_dot_v;

//	vector = minus(camera, figure->center);
	if ((d_dot_v = dot_product(direction, figure->direction)) == 0)
		t->t1 = T_MAX;
	else
		t->t1 = (-dot_product(camera, figure->direction)) / d_dot_v;
	t->t2 = T_MAX;
}
