/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   image.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mnosko <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/24 19:18:09 by mnosko            #+#    #+#             */
/*   Updated: 2018/10/24 19:18:12 by mnosko           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/rtv1.h"

void			put_pixel(SDL_Surface *surface, int x,
				int y, unsigned int pixel)
{
	Uint32 *pixels;

	pixels = (Uint32 *)surface->pixels;
	pixels[(y * surface->w) + x] = pixel;
}

t_color			create_color(char *str)
{
	char	**split;
	t_color	color;
	int		i;

	str = ft_strtrim(str);
	split = ft_strsplit(++str, ',');
	i = 0;
	while (i < 3)
	{
		if (split[i] == NULL)
		{
			ft_delete(split);
			return ((t_color){255, 255, 255});
		}
		i++;
	}
	free(--str);
	color.r = ft_atoi(split[0]) > 255 ||
			ft_atoi(split[0]) < 0 ? 255 : (Uint8)ft_atoi(split[0]);
	color.g = ft_atoi(split[1]) > 255 ||
			ft_atoi(split[1]) < 0 ? 255 : (Uint8)ft_atoi(split[1]);
	color.b = ft_atoi(split[2]) > 255 ||
			ft_atoi(split[2]) < 0 ? 255 : (Uint8)ft_atoi(split[2]);
	ft_delete(split);
	return (color);
}

unsigned int	create_rgb(t_color color, double i)
{
	color.r *= i;
	if (color.r > 255)
		color.r = 255;
	color.g *= i;
	if (color.g > 255)
		color.g = 255;
	color.b *= i;
	if (color.b > 255)
		color.b = 255;
	return ((((unsigned)color.r & 0xff) << 16)
			+ (((unsigned)color.g & 0xff) << 8)
			+ ((unsigned)color.b & 0xff));
}
