/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   screen.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mnosko <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/24 19:20:56 by mnosko            #+#    #+#             */
/*   Updated: 2018/10/24 19:20:58 by mnosko           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/rtv1.h"

t_vec3	canvas_to_viewport(int x, int y, t_rtv1 *rtv1)
{
	t_vec3	ret_val;

	ret_val.x = (double)x * rtv1->view[0] / WINDOW_W;
	ret_val.y = (double)y * rtv1->view[1] / WINDOW_W;
	ret_val.z = rtv1->d;
	return (ret_val);
}
