/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error_management.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mnosko <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/24 19:17:30 by mnosko            #+#    #+#             */
/*   Updated: 2018/10/24 19:17:33 by mnosko           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/rtv1.h"

int		if_split_null(char **split, char **line)
{
	if (split == NULL)
	{
		delete_before_leaving(line, split);
		return (TRUE);
	}
	if (*split == NULL)
	{
		delete_before_leaving(line, split);
		return (TRUE);
	}
	return (FALSE);
}

int		if_symbol(char **split, char **line, char *str)
{
	if (equals(str, "{"))
	{
		delete_before_leaving(line, split);
		return (TRUE);
	}
	return (FALSE);
}

void	put_error(char *str)
{
	ft_putendl_fd(str, 2);
	system("leaks rtv1");
	exit(0);
}
