/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ray.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mnosko <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/24 19:20:05 by mnosko            #+#    #+#             */
/*   Updated: 2018/10/24 19:20:07 by mnosko           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <SDL_types.h>
#include "../include/rtv1.h"

void			draw(t_rtv1 *rtv1, SDL_Surface *surface)
{
	int				x;
	int				y;
	t_vec3			direction;
	unsigned int	color;

	change_figure_direction(rtv1->figure);
	x = -WINDOW_W / 2;
	while (x < WINDOW_W / 2)
	{
		y = -WINDOW_H / 2;
		while (y < WINDOW_H / 2)
		{
			direction = canvas_to_viewport(x, y, rtv1);
			direction = divide(direction, vector_length(direction));
			color = trace_ray(rtv1->camera_pos,
					rotate(direction, rtv1->camera_rot, 1), rtv1);
			put_pixel(surface, WINDOW_W / 2 + x, WINDOW_H / 2 - y - 1, color);
			y++;
		}
		x++;
	}
}

void		change_figure_direction(t_figure *figures)
{
	t_figure *figure;

	figure = figures;
	while (figure)
	{
		figure->direction = rotate((t_vec3){0, 1, 0}, figure->rotation, 1);
		figure = figure->next;
	}
}

unsigned int	trace_ray(t_vec3 camera, t_vec3 direction, t_rtv1 *rtv1)
{
	t_figure	*closest_figure;
	double		closest_t;
	t_vec3		n;
	t_vec3		p;

	closest_t = T_MAX;
	closest_figure = closest_intersection(camera, direction,
			&closest_t, rtv1->figure);
	if (closest_figure == NULL)
		return (0x00000);
	p = plus(camera, multiply(direction, closest_t));
	p = (t_vec3){p.x - 0.00000001, p.y - 0.00000001, p.z - 0.00000001};
	n = get_normal(p, closest_figure, closest_t, (t_ray){camera, direction});
	return (create_rgb(closest_figure->color,
			calculate_light(rtv1, (t_light_variables){n, p}, closest_figure,
			multiply(direction, -1))));
}

t_figure		*closest_intersection(t_vec3 camera, t_vec3 direction,
				double *closest_t, t_figure *figure)
{
	t_figure	*closest_figure;
	t_figure	*figures;
	t_t			t;

	closest_figure = NULL;
	figures = figure;
	while (figures)
	{
		if (figures->radius <= 0)
		{
			figures = figures->next;
			continue;
		}
		t = current_figure_intersection(figures, minus(camera, figures->center), direction);
		if ((t.t1 > T_MIN && t.t1 < T_MAX) && t.t1 < *closest_t)
		{
			*closest_t = t.t1;
			closest_figure = figures;
		}
		if ((t.t2 > T_MIN && t.t2 < T_MAX) && t.t2 < *closest_t)
		{
			*closest_t = t.t2;
			closest_figure = figures;
		}
		figures = figures->next;
	}
	return (closest_figure);
}

t_t				current_figure_intersection(t_figure *figure,
				t_vec3 camera, t_vec3 direction)
{
	t_t t;

	if (figure->type == CONE)
		ray_intersect_cone(figure, &t, camera, direction);
	else if (figure->type == CYLINDER)
		ray_intersect_cylinder(figure, &t, camera, direction);
	else if (figure->type == SPHERE)
	{
		ray_intersect_sphere(figure, &t, camera, direction);
	}
	else if (figure->type == PLANE)
		ray_intersect_plane(figure, &t, camera, direction);
	return (t);
}
