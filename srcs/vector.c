/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vector.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mnosko <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/24 19:21:48 by mnosko            #+#    #+#             */
/*   Updated: 2018/10/24 19:21:50 by mnosko           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/rtv1.h"

t_vec3	multiply(t_vec3 vector, double multiplier)
{
	vector.x *= multiplier;
	vector.y *= multiplier;
	vector.z *= multiplier;
	return (vector);
}

t_vec3	divide(t_vec3 vector, double divider)
{
	vector.x /= divider;
	vector.y /= divider;
	vector.z /= divider;
	return (vector);
}

t_vec3	plus(t_vec3 a, t_vec3 b)
{
	t_vec3 ret_vec;

	ret_vec.x = a.x + b.x;
	ret_vec.y = a.y + b.y;
	ret_vec.z = a.z + b.z;
	return (ret_vec);
}

t_vec3	minus(t_vec3 a, t_vec3 b)
{
	t_vec3 ret_vec;

	ret_vec.x = a.x - b.x;
	ret_vec.y = a.y - b.y;
	ret_vec.z = a.z - b.z;
	return (ret_vec);
}
