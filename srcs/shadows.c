/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   shadows.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mnosko <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/24 19:21:10 by mnosko            #+#    #+#             */
/*   Updated: 2018/10/24 19:21:14 by mnosko           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/rtv1.h"

int		shadows(t_ray ray, t_figure *figures, double t_min, int t_max)
{
	t_figure	*closest_figure;
	t_figure	*figure;
	t_t			t;
	double		closest_t;

	closest_figure = NULL;
	closest_t = T_MAX;
	figure = figures;
	while (figure)
	{
		if (figure->radius <= 0)
		{
			figure = figure->next;
			continue;
		}
//		ray.camera = minus(ray.camera, figure->center);
		t = current_figure_intersection(figure, minus(ray.camera, figure->center), ray.direction);
		if ((t.t1 >= t_min && t.t1 < t_max) && t.t1 < closest_t)
		{
			closest_t = t.t1;
			closest_figure = figure;
		}
		if ((t.t2 >= t_min && t.t2 < t_max) && t.t2 < closest_t)
		{
			closest_t = t.t2;
			closest_figure = figure;
		}
		figure = figure->next;
	}
	return (closest_figure != NULL ? FALSE : TRUE);
}
