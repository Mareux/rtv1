/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rotation.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mnosko <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/24 19:20:43 by mnosko            #+#    #+#             */
/*   Updated: 2018/10/24 19:20:45 by mnosko           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/rtv1.h"

t_vec3	rotate(t_vec3 vector, t_vec3 degree, int inversion)
{
	rotation_x(&vector, degree.x, inversion);
	rotation_y(&vector, degree.y, inversion);
	rotation_z(&vector, degree.z, inversion);
	return (vector);
}

void	rotation_x(t_vec3 *vector, double degree, int inversion)
{
	double radians;
	double tmp;

	radians = degree * (PI / 180);
	tmp = (*vector).y * cos(radians * inversion)
			+ (*vector).z * sin(radians * inversion);
	(*vector).z = -(*vector).y * sin(radians * inversion)
			+ (*vector).z * cos(radians * inversion);
	(*vector).y = tmp;
}

void	rotation_y(t_vec3 *vector, double degree, int inversion)
{
	double radians;
	double tmp;

	radians = degree * (PI / 180);
	tmp = (*vector).x * cos(radians * inversion)
			- (*vector).z * sin(radians * inversion);
	(*vector).z = (*vector).x * sin(radians * inversion)
			+ (*vector).z * cos(radians * inversion);
	(*vector).x = tmp;
}

void	rotation_z(t_vec3 *vector, double degree, int inversion)
{
	double radians;
	double tmp;

	radians = degree * (PI / 180);
	tmp = (*vector).x * cos(radians * inversion)
			- (*vector).y * sin(radians * inversion);
	(*vector).y = (*vector).x * sin(radians * inversion)
			+ (*vector).y * cos(radians * inversion);
	(*vector).x = tmp;
}
