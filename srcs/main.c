/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mnosko <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/24 19:18:45 by mnosko            #+#    #+#             */
/*   Updated: 2018/10/24 19:18:47 by mnosko           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/rtv1.h"
#include <fcntl.h>
#include <zconf.h>

int		main(int argc, char **argv)
{
	SDL_Surface		*surface;
	SDL_Window		*window;
	t_rtv1			*rtv1;
	int				fd;

	if (argc != 2)
		return (0);
	fd = open(argv[1], O_RDONLY);
	init(&rtv1);
	parser(fd, rtv1);
	close(fd);
	SDL_Init(SDL_INIT_VIDEO);
	window = SDL_CreateWindow("Test", SDL_WINDOWPOS_UNDEFINED,
			SDL_WINDOWPOS_UNDEFINED, WINDOW_W, WINDOW_H, 0);
	surface = SDL_GetWindowSurface(window);
	draw(rtv1, surface);
	SDL_UpdateWindowSurface(window);
	key_hooks(rtv1, surface, window);
	SDL_DestroyWindow(window);
	SDL_Quit();
	return (EXIT_SUCCESS);
}
