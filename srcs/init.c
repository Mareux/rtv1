/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mnosko <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/24 19:18:24 by mnosko            #+#    #+#             */
/*   Updated: 2018/10/24 19:18:26 by mnosko           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/rtv1.h"

void	init(t_rtv1 **rtv1)
{
	*rtv1 = malloc(sizeof(t_rtv1));
	(*rtv1)->d = 1;
	(*rtv1)->view[0] = 1;
	(*rtv1)->view[1] = 1;
	(*rtv1)->camera_pos = create_vector_from_str("[0, 0, 0]");
	(*rtv1)->camera_rot = create_vector_from_str("[0, 0, 0]");
	(*rtv1)->figure = NULL;
	(*rtv1)->light = NULL;
}
