/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   normal.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mnosko <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/24 19:18:56 by mnosko            #+#    #+#             */
/*   Updated: 2018/10/24 19:18:57 by mnosko           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/rtv1.h"

t_vec3 get_normal(t_vec3 p, t_figure *closest_figure, double closest_t, t_ray ray)
{
	t_vec3 normal;

	if (closest_figure->type == SPHERE)
		normal = get_sphere_normal(p, closest_figure);
	else if (closest_figure->type == CYLINDER)
		normal = get_cylinder_normal(p, closest_figure, closest_t, ray);
	else if (closest_figure->type == CONE)
		normal = get_cone_normal(p, closest_figure, closest_t, ray);
	else
		normal = get_plane_normal(ray.direction, closest_figure);
	return (normal);
}

t_vec3		get_sphere_normal(t_vec3 p, t_figure *closest_figure)
{
	t_vec3 normal;

	normal = minus(p, closest_figure->center);
	normal = divide(normal, vector_length(normal));
	return (normal);
}

t_vec3	get_cylinder_normal(t_vec3 p, t_figure *closest_figure, double closest_t, t_ray ray)
{
	t_vec3	normal;

	double m = dot_product(ray.direction, closest_figure->direction)
			* closest_t + dot_product(minus(ray.camera,
					closest_figure->center), closest_figure->direction);
	normal = minus(minus(p, closest_figure->center),
			multiply(closest_figure->direction, m));
	normal = divide(normal, vector_length(normal));
	return (normal);
}

t_vec3	get_cone_normal(t_vec3 p, t_figure *closest_figure, double closest_t, t_ray ray)
{
	t_vec3 normal;

	double m = dot_product(ray.direction, closest_figure->direction)
			   * closest_t + dot_product(minus(ray.camera,
			   closest_figure->center), closest_figure->direction);
	normal = minus(minus(p, closest_figure->center),
			multiply(closest_figure->direction,
					(1 + closest_figure->radians * closest_figure->radians) * m));
	normal = divide(normal, vector_length(normal));
	return (normal);
}

t_vec3	get_plane_normal(t_vec3 direction, t_figure *closest_figure)
{
	t_vec3 normal;

	if (dot_product(direction, closest_figure->direction) > 0)
		normal = closest_figure->direction;
	else
		normal = multiply(closest_figure->direction, -1);
	normal = divide(normal, vector_length(normal));
	return (normal);
}
