/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mnosko <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/24 19:21:35 by mnosko            #+#    #+#             */
/*   Updated: 2018/10/24 19:21:37 by mnosko           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/rtv1.h"
#include "../get_next_line.h"

void	delete_before_leaving(char **line, char **split)
{
	if (*line != NULL)
	{
		free(*line);
		*line = NULL;
	}
	if (split != NULL)
		ft_delete(split);
}

void	ft_delete(char **ar)
{
	int i;

	i = 0;
	while (ar[i])
	{
		ft_strdel(&ar[i]);
		i++;
	}
	free(ar);
	*ar = NULL;
}

char	**get_parser_info(char **line, int *gnl, int fd)
{
	char **split;

	*gnl = get_next_line(fd, line);
	split = ft_strsplit(*line, ':');
	return (split);
}
