/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mnosko <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/24 19:19:35 by mnosko            #+#    #+#             */
/*   Updated: 2018/10/24 19:19:37 by mnosko           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/rtv1.h"
#include "../get_next_line.h"

void	parser(int fd, t_rtv1 *rtv1)
{
	char	*line;
	char	**split;
	int		error;

	while ((error = get_next_line(fd, &line)))
	{
		if (error < 0)
			put_error("Wrong file!");
		split = ft_strsplit(line, ':');
		if (if_split_null(split, &line))
			put_error("Wrong number of lines, or wrong file");
		if (equals(split[0], "position"))
			rtv1->camera_pos = create_vector_from_str(split[1]);
		else if (equals(split[0], "rotation"))
			rtv1->camera_rot = create_vector_from_str(split[1]);
		if (split[1] && equals(split[1], "{"))
		{
			if (parser_for_objects(split, fd, rtv1) < 0)
				put_error("Wrong number of lines, or wrong file");
		}
		delete_before_leaving(&line, split);
	}
}

int		parser_for_objects(char **split, int fd, t_rtv1 *rtv1)
{
	int error;

	error = 0;
	if (equals(split[0], "light"))
		error = add_light(fd, &rtv1->light);
	else if (equals(split[0], "cone"))
		error = add_figure(fd, &rtv1->figure, CONE);
	else if (equals(split[0], "sphere"))
		error = add_figure(fd, &rtv1->figure, SPHERE);
	else if (equals(split[0], "cylinder"))
		error = add_figure(fd, &rtv1->figure, CYLINDER);
	else if (equals(split[0], "plane"))
		error = add_figure(fd, &rtv1->figure, PLANE);
	return (error);
}

int		add_figure(int fd, t_figure **figure, enum e_figure_type type)
{
	char	**split;
	char	*line;
	int		gnl;

	pop_figure(figure, type);
	split = get_parser_info(&line, &gnl, fd);
	if (if_split_null(split, &line))
		return (-1);
	while (!equals(split[0], "}") && gnl > 0)
	{
//		if (if_symbol(split, &line, "{"))
//			return (-2);
		if (equals(split[0], "center"))
			(*figure)->center = create_vector_from_str(split[1]);
		else if (equals(split[0], "rotation"))
			(*figure)->rotation = create_vector_from_str(split[1]);
		else if (equals(split[0], "radius"))
			(*figure)->radius = split[1] != NULL ? ft_atof(split[1]) : 1;
		else if (equals(split[0], "specular"))
			(*figure)->specular = ft_atof(split[1]);
		else if (equals(split[0], "degree"))
			(*figure)->degree = ft_atof(split[1]);
		else if (equals(split[0], "color"))
			(*figure)->color = create_color(split[1]);
		delete_before_leaving(&line, split);
		split = get_parser_info(&line, &gnl, fd);
		if (if_split_null(split, &line))
			return (-1);
	}
	delete_before_leaving(&line, split);
	return (TRUE);
}

int		add_light(int fd, t_light **light)
{
	char	**split;
	char	*line;
	int		gnl;

	pop_light(light);
	split = get_parser_info(&line, &gnl, fd);
	if (if_split_null(split, &line))
		return (-1);
	while (!equals(split[0], "}") && gnl > 0)
	{
//		if (if_symbol(split, &line, "{"))
//			return (-2);
		if (equals(split[0], "position"))
			(*light)->position = create_vector_from_str(split[1]);
		else if (equals(split[0], "intensity"))
			(*light)->intensity = split[1] != NULL ? ft_atof(split[1]) : 0.0;
		else if (equals(split[0], "type"))
			get_light_type(*light, split[1]);
		delete_before_leaving(&line, split);
		split = get_parser_info(&line, &gnl, fd);
		if (if_split_null(split, &line))
			return (-1);
	}
	delete_before_leaving(&line, split);
	return (TRUE);
}

void	get_light_type(t_light *light, char *value)
{
	if (value == NULL)
	{
		light->type = NONE;
		return ;
	}
	if (equals(value, "ambient"))
		light->type = AMBIENT;
	else if (equals(value, "point"))
		light->type = POINT;
}
