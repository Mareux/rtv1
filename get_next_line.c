/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mnosko <mnosko@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/20 12:19:13 by mnosko            #+#    #+#             */
/*   Updated: 2018/02/21 14:05:56 by mnosko           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"

t_gnl	*check_fd(t_gnl **element, int fd)
{
	t_gnl *new;
	t_gnl *tmp_point;

	tmp_point = *element;
	while (tmp_point != NULL)
	{
		if ((tmp_point)->fd == fd)
			return (tmp_point);
		tmp_point = tmp_point->next;
	}
	if ((new = malloc(sizeof(t_gnl))) == NULL)
		return (NULL);
	new->fd = fd;
	new->content = NULL;
	new->length = 0;
	new->next = (*element);
	(*element) = new;
	return (*element);
}

char	*func_to_strncpy(char *dst, const char *src, size_t len)
{
	int		i;
	int		k;

	i = 0;
	k = 0;
	while (i < (int)len)
	{
		dst[i] = src[k];
		i++;
		k++;
	}
	dst[i] = '\0';
	return (dst);
}

int		func_to_read(t_gnl **curr, int fd)
{
	int		ret_value;
	size_t	offset;
	char	*new;
	size_t	full_len;

	offset = (*curr)->content != NULL ? (*curr)->length : 0;
	full_len = BUFF_SIZE + offset;
	if ((new = malloc((full_len + 1))) == NULL)
		return (0);
	if (offset != 0)
		func_to_strncpy(new, (*curr)->content, (size_t)(*curr)->length);
	free((*curr)->content);
	(*curr)->content = new;
	ret_value = (int)read(fd, ((*curr)->content + offset), BUFF_SIZE);
	if (ret_value < 0)
		return (ret_value);
	(*curr)->length += ret_value;
	new[full_len] = '\0';
	return (ret_value);
}

int		func_to_save(t_gnl **curr, char **line, const char *n, int ret_value)
{
	char	*new;
	size_t	line_len;

	if ((*curr)->length > 0 && ret_value == 0)
	{
		if ((*line = malloc((*curr)->length + 1)) == NULL)
			return (0);
		*line = func_to_strncpy(*line, (*curr)->content, (*curr)->length);
		ft_strdel(&(*curr)->content);
		(*curr)->length = 0;
		return (1);
	}
	line_len = (n - (*curr)->content);
	if ((*line = malloc(line_len + 1)) == NULL)
		return (0);
	func_to_strncpy(*line, (*curr)->content, line_len);
	(*curr)->length = (*curr)->length - line_len - 1;
	if ((new = malloc((*curr)->length + 1)) == NULL)
		return (0);
	func_to_strncpy(new, (*curr)->content + (line_len + 1), (*curr)->length);
	free((*curr)->content);
	(*curr)->content = new;
	return (1);
}

int		get_next_line(const int fd, char **line)
{
	static t_gnl	*element;
	t_gnl			*curr;
	char			*n;
	int				ret_value;

	if (fd < 0 || BUFF_SIZE < 0 || !line)
		return (-1);
	curr = check_fd(&element, fd);
	if (curr->content != NULL &&
		(n = ft_memchr(curr->content, '\n', (size_t)curr->length)) != 0)
		return (func_to_save(&curr, line, n, 1));
	while ((ret_value = func_to_read(&curr, fd)) > 0)
	{
		if ((n = ft_memchr(curr->content, '\n', (size_t)curr->length)) != 0)
			return (func_to_save(&curr, line, n, 1));
	}
	if (ret_value < 0)
		return (-1);
	if (curr->length > 0 && ret_value == 0)
		return (func_to_save(&curr, line, 0, ret_value));
	return (0);
}
